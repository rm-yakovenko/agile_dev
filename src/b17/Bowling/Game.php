<?php

namespace b17\Bowling;

class Game
{
    private $score = 0;

    private $throws = [];

    private $throwIndex = 0;

    public function getScore()
    {
        return $this->score;
    }

    public function add($points)
    {
        $this->score += $points;
        $this->throws[$this->throwIndex++] = $points;
    }

    public function getScoreForFrame($frame)
    {
        $ball = 0;
        $score = 0;
        for ($currentFrame = 0; $currentFrame < $frame; ++$currentFrame) {
            $firstThrow = $this->throws[$ball++];
            $secondThrow = $this->throws[$ball++];
            $score += $firstThrow + $secondThrow;
        }

        return $score;
    }
}
