<?php

namespace b17\DIP;

interface SwitchableDevice
{
  public function turnOn();

  public function turnOff();

  public function isTurnedOn();
}
