<?php

use b17\Bowling\Game;

class GameTest extends \PHPUnit_Framework_TestCase
{
    public function testOneThrow()
    {
        $game = new Game();
        $game->add(5);
        $this->assertEquals(5, $game->getScore());
    }

    public function testTwoThrowsNoMark()
    {
      $game = new Game();
      $game->add(5);
      $game->add(4);
      $this->assertEquals(9, $game->getScore());
    }

    public function testFourThrowsNoMark()
    {
      $game = new Game();
      $game->add(4);
      $game->add(5);
      $game->add(7);
      $game->add(2);
      $this->assertEquals(18, $game->getScore());
      $this->assertEquals(9, $game->getScoreForFrame(1));
      $this->assertEquals(18, $game->getScoreForFrame(2));
    }

    public function testSimpleSpare()
    {
      $game = new Game();
      $game->add(3);
      $game->add(7);
      $game->add(3);
      $this->assertEquals(13, $game->getScoreForFrame(1));
    }
}
