<?php

namespace b17\Shape;

interface ShapeInterface
{
  public function draw();
}
