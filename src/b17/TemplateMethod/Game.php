<?php

namespace b17\TemplateMetod;

abstract class Game
{
    abstract public function initGame();

    public function play()
    {
        $this->initGame();
        $this->endGame();
    }

    abstract public function endGame();
}
