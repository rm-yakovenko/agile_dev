<?php

use b17\DIP\Lamp;

/**
 *
 */
class LamTest extends \PHPUnit_Framework_TestCase
{
  public function testTurnedOffByDefault()
  {
    $lamp = new Lamp();
    $this->assertFalse($lamp->isTurnedOn());
  }

  public function testTurnOn()
  {
    $lamp = new Lamp();
    $lamp->turnOn();

    $this->assertEquals(true, $lamp->isTurnedOn());
  }

  public function testTurnOff()
  {
    $lamp = new Lamp();
    $lamp->turnOn();
    $lamp->turnOff();

    $this->assertFalse($lamp->isTurnedOn());
  }
}
