<?php

namespace b17\Shape\Rectangle;

class Rectangle implements ShapeInterface
{
    private $width;

    private $height;

    public function draw()
    {
    }

    /**
     * Get the value of Width.
     *
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set the value of Width.
     *
     * @param mixed width
     *
     * @return self
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Get the value of Height.
     *
     * @return mixed
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set the value of Height.
     *
     * @param mixed height
     *
     * @return self
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }
}
