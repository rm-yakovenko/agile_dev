<?php

namespace b17\Shaoe\Circle;

class Circle implements ShapeInterface
{
  /**
  * @var string
  */
  private $radius;  


  public function draw()
  {

  }

    /**
     * Get the value of Radius
     *
     * @return string
     */
    public function getRadius()
    {
        return $this->radius;
    }

    /**
     * Set the value of Radius
     *
     * @param string radius
     *
     * @return self
     */
    public function setRadius($radius)
    {
        $this->radius = $radius;

        return $this;
    }

}
