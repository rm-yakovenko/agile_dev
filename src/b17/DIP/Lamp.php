<?php

namespace b17\DIP;

use b17\DIP\LampInterface;

class Lamp implements SwitchableDevice
{
  private $isTurnedOn = false;

  public function turnOn()
  {
    $this->isTurnedOn = true;
  }

  public function turnOff()
  {
    $this->isTurnedOn = false;
  }

  public function isTurnedOn()
  {
    return $this->isTurnedOn;
  }
}
