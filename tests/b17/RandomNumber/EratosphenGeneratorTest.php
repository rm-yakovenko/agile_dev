<?php

use b17\RandomNumber\EratosphenGenerator;

class EratosphenGeneratorTest  extends \PHPUnit_Framework_TestCase
{
    public function testGenerate()
    {
        $generator = new EratosphenGenerator();
        $this->assertEquals($generator->generate(11), [2, 3, 5, 7, 11]);
    }
}
