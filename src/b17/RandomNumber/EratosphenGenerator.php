<?php

namespace b17\RandomNumber;

class EratosphenGenerator
{
    public function generate($max)
    {
        $isCrossed = [];
        for ($i = 2; $i <= $max; ++$i) {
            $isCrossed[$i] = false;
        }

        for ($i = 2; $i <= intval(sqrt($max)); ++$i) {
            if ($isCrossed[$i]) {
                continue;
            }
            for ($j = 2 * $i; $j <= $max; $j += $i) {
                $isCrossed[$j] = true;
            }
        }

        $primeNumbers = [];
        for ($i = 2; $i <= $max; ++$i) {
            if (!$isCrossed[$i]) {
                $primeNumbers[] = $i;
            }
        }

        return $primeNumbers;
    }
}
