<?php

namespace b17\DIP;

use b17\DIP\Lamp;

class Button
{
  private $isPolled = false;

  /**
   * Lamp
   *
   * @var Lamp
   **/
  private $lamp;

  public function poll()
  {
    if ($this->isPolled) {
      $this->lamp->turnOff();
    } else {
      $this->lamp->turnOn();
    }
  }
}
